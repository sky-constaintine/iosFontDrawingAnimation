//
//  AppDelegate.h
//  FontAnimation
//
//  Created by etrump on 12/15/15.
//  Copyright © 2015 sky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

