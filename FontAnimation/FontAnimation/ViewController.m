//
//  ViewController.m
//  FontAnimation
//
//  Created by etrump on 12/15/15.
//  Copyright © 2015 sky. All rights reserved.
//

#import "ViewController.h"
#import "MyWorldLayer.h"

@interface ViewController ()
@property(strong, nonatomic) UIButton* button;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.button addTarget:self action:@selector(beginDraw:) forControlEvents:UIControlEventTouchUpInside];
    [self.button setTitle:@"Click" forState:UIControlStateNormal];
    [self.button setBackgroundColor:[UIColor blueColor]];
    self.button.frame = CGRectMake(0, 400, 100, 60);
    [self.view addSubview:self.button];
}

-(void) beginDraw : (UIButton*)sender
{
    for( NSInteger i=0; i < [self.view.layer sublayers].count; i++ )
    {
        CALayer* layer = (CALayer*)[[self.view.layer sublayers] objectAtIndex:i];
        if( [layer isKindOfClass:[MyWorldLayer class]] )
            [layer removeFromSuperlayer];
    }
    
    
    [MyWorldLayer createAnimation:@"不知道" font:[UIFont systemFontOfSize:50] rect:[UIScreen mainScreen].bounds view:self.view color:[UIColor redColor]];
    
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
