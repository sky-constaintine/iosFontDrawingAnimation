//
//  MyWorldLayer.h
//  FontAnimation
//
//  Created by etrump on 12/15/15.
//  Copyright © 2015 sky. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface MyWorldLayer : CALayer

+ (void) createAnimation : (NSString*)str font:(UIFont*)font rect:(CGRect)rect view:(UIView*)view color:(UIColor*)color;

@end
